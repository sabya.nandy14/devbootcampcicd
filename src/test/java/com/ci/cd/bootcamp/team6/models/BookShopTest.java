package com.ci.cd.bootcamp.team6.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BookShopTest {

    @Test
    public void returnsBookInstance() {
        BookShop bookShop = new BookShop("bookShop");
        assertNotNull(bookShop);
    }

    @Test
    public void bookShopShouldReturnBookName() {
        BookShop bookShop = new BookShop("harry potter");
        assertEquals(bookShop.getName(), "harry potter");
    }
}
