package com.ci.cd.bootcamp.team6.controllers;

import com.ci.cd.bootcamp.team6.models.BookShop;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class BookShopControllerTest {

    @Test
    public void bookShopControllerInstanceIsReturned() {
        BookShopController bookShopController = new BookShopController();
        assertNotNull(bookShopController);
    }

    @Test
    public void bookShopControllerReturnCorrectBook(){
        BookShopController bookShopController = new BookShopController();
        Assert.assertEquals(bookShopController.application().getName(), "Sample BookShop");
    }
}
