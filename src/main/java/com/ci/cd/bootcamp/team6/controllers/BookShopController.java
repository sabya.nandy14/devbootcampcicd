package com.ci.cd.bootcamp.team6.controllers;

import com.ci.cd.bootcamp.team6.models.BookShop;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookShopController {
    private BookShop bookShop = new BookShop("Sample BookShop");

    @RequestMapping("/application")
    public BookShop application() {
        return bookShop;
    }
}
