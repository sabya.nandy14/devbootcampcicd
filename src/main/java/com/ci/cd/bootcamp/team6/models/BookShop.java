package com.ci.cd.bootcamp.team6.models;

public class BookShop {
    public BookShop(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return this.name;
    }
}